# Welcome to Java-Calculator by pa1007

This is my first calculator I have made, it is in JAVA and can do some simple operation (+, -, *, /, ²,...) and will do in the **future** more (Check the releases and changelog)

## Design :
The Calculator is in Java and use JavaFX for been shown and use the  **[JFoenix](https://github.com/jfoenixadmin/JFoenix)**  library : 

![Design of the calculator](https://puu.sh/ASYfv/7e5b7f1aa1.png)

## Evolution : 
The application will evolve in the near future and may be susceptible to change ! You can test it and report any error in the ISSUE tab and offers some change to fix it !