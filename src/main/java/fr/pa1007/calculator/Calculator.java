package fr.pa1007.calculator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.pa1007.calculator.constant.Log;
import fr.pa1007.calculator.constant.OperatorTypes;
import fr.pa1007.calculator.controller.MainSceneController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Calculator extends Application {

    static final         Gson       GSON     =
            new GsonBuilder().serializeNulls().setPrettyPrinting().create();
    private static final Calculator INSTANCE = new Calculator();
    private static final List<Log>  logList  = new ArrayList<>();

    @Override
    public void start(Stage primaryStage) throws IOException {
        launchCalculator();
        System.out.println(getLogDirectory());
        this.loadLog();
    }

    public void loadLog() {
        Path settingsDirectory = getLogDirectory();

        if (Files.notExists(settingsDirectory)) {
            System.out.print("No Settings loaded since the file containing the Log does not exists!");
            return;
        }

        try (Reader reader = Files.newBufferedReader(settingsDirectory)) {
            Log[] settings = GSON.fromJson(reader, Log[].class);


            logList.addAll(Arrays.stream(settings).collect(Collectors.toList()));


            System.out.print("There is " + logList.size() + " log register");
        }
        catch (IOException e) {
            System.out.print("Unable to load log:" + e);
        }
    }

    public static List<Log> getLogList() {
        return logList;
    }

    private static Path getLogDirectory() {
        return Paths.get((System.getenv("USERPROFILE")) + ("\\My Documents\\calculator_log\\")).resolve(
                "Log-" + LocalDate.now() + "-.json");
    }

    public static Calculator getINSTANCE() {
        return INSTANCE;
    }

    public static void saveLog() {
        Path spawnFile = getLogDirectory();
        try {
            System.out.print("Saving Settings");

            Path parentDir = spawnFile.getParent();

            if (!Files.isDirectory(parentDir)) {
                Files.createDirectories(parentDir);
            }

            try (Writer writer = Files.newBufferedWriter(spawnFile)) {
                GSON.toJson(logList, writer);
            }

            System.out.println("Successfully saved {} log !" + logList.size());
        }
        catch (IOException e) {
            System.out.print("Unable to save spawn:" + e);
        }
    }

    /**
     * To launch the calculator from a given primary stage
     *
     * @param primary {@link javafx.stage.Stage Stage} to launch in this stage !
     * @throws IOException exception throw if the scene file is not present
     */
    public static void launchCalculator(Stage primary) throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Calculator.class.getResource("/scene/mainScene.fxml"));
        HBox                hBox       = loader.load();
        MainSceneController controller = loader.getController();
        Scene               scene      = new Scene(hBox);
        scene.getStylesheets().add(Calculator.class.getResource("/css/grow.css").toExternalForm());
        primary.setScene(scene);
        primary.setResizable(false);
        primary.getIcons().add(new Image(Calculator.class.getResourceAsStream("/image/AppIcon.png")));
        primary.setTitle("Calculator");
        controller.setMainStage(primary);
        primary.show();
    }

    /**
     * to launch the calculator in a new stage !
     */
    public static void launchCalculator() throws IOException {
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(Calculator.class.getResource("/scene/mainScene.fxml"));
        HBox                hBox       = loader.load();
        MainSceneController controller = loader.getController();
        Scene               scene      = new Scene(hBox);
        scene.getStylesheets().add(Calculator.class.getResource("/css/grow.css").toExternalForm());
        Stage primary = new Stage();
        primary.setScene(scene);
        primary.setResizable(false);
        primary.getIcons().add(new Image(Calculator.class.getResourceAsStream("/image/AppIcon.png")));
        primary.setTitle("Calculator");
        controller.setMainStage(primary);
        primary.show();
    }

    /**
     * To do an operation with an type
     *
     * @param operatorTypes set an operationType main specific
     * @param op            the number to get the result in a String Array form
     * @return double operation or with wrong {@link fr.pa1007.calculator.constant.OperatorTypes OperatorTypes} can return Double.MIN_VALUE if error
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double operation(OperatorTypes operatorTypes, String[] op) throws ArithmeticException {
        List<Double> number = new ArrayList<>();
        for (String anOp : op) {
            number.add(Double.valueOf(anOp));
        }
        Double[] numbers = number.toArray(new Double[0]);
        switch (operatorTypes) {
            case ADD:
                return Calculator.addition(numbers);
            case MULTIPLY:
                return Calculator.multiply(numbers);
            case MINUS:
                return Calculator.subtract(numbers);
            case DIVIDE:
                return Calculator.divide(numbers);
            default:
                return Double.MIN_VALUE;
        }
    }

    /**
     * To do an operation with an  type
     *
     * @param operatorTypes set an operationType main specific
     * @param number        the number to get the result
     * @return double operation or with wrong {@link fr.pa1007.calculator.constant.OperatorTypes OperatorTypes} can return Double.MIN_VALUE
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double operation(OperatorTypes operatorTypes, double... number) throws ArithmeticException {
        switch (operatorTypes) {
            case ADD:
                return addition(number);
            case MULTIPLY:
                return multiply(number);
            case MINUS:
                return subtract(number);
            case DIVIDE:
                return divide(number);
            default:
                return Double.MIN_VALUE;
        }
    }

    /**
     * To get a result of an number at the power done
     *
     * @param number the number to get the result
     * @param power  the power given
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double power(double number, double power) throws ArithmeticException {
        return Math.pow(number, power);
    }

    /**
     * Divide Operation with array double
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double divide(Double[] numbers) throws ArithmeticException {
        double result = 0;
        for (double number : numbers) {
            result /= number;
        }

        return result;
    }

    /**
     * addition Operation with array double
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double addition(Double[] numbers) throws ArithmeticException {
        double result = 0;
        for (double number : numbers) {
            result += number;
        }
        return result;
    }

    /**
     * Subtract Operation with array double
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double subtract(Double[] numbers) throws ArithmeticException {
        double result = 0;
        for (double number : numbers) {
            result -= number;
        }
        return result;
    }

    /**
     * Multiply Operation with array double
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double multiply(Double[] numbers) throws ArithmeticException {
        double result = 0;
        for (double number : numbers) {
            result *= number;
        }

        return result;
    }

    /**
     * Subtract Operation with double secession
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double subtract(double... numbers) throws ArithmeticException {
        double result;
        if (numbers.length == 2) {
            return numbers[0] - numbers[1];
        }
        else {
            result = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                result -= numbers[i];
            }
        }

        return result;
    }

    /**
     * Multiply Operation with double secession
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double multiply(double... numbers) throws ArithmeticException {
        double result = 1;
        for (double number : numbers) {
            result *= number;
        }

        return result;
    }

    /**
     * Divide Operation with double secession
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    public static double divide(double... numbers) throws ArithmeticException {
        double result;
        if (numbers.length == 2) {
            return numbers[0] / numbers[1];
        }
        else {
            result = numbers[0];
            for (int i = 1; i < numbers.length; i++) {
                result /= numbers[i];
            }
        }

        return result;
    }

    /**
     * addition Operation with double secession
     *
     * @param numbers the number to get the result
     * @return an double result
     * @throws ArithmeticException exception if there is an unexpected symbol
     */
    private static double addition(double... numbers) throws ArithmeticException {
        double result = 0;
        for (double number : numbers) {
            result += number;
        }
        return result;
    }

}
