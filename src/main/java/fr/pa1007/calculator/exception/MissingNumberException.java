package fr.pa1007.calculator.exception;

public class MissingNumberException extends Exception {

    public MissingNumberException() {
        super();
    }

    public MissingNumberException(String message) {
        super(message);
    }

    public MissingNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public MissingNumberException(Throwable cause) {
        super(cause);
    }
}