package fr.pa1007.calculator.constant;

import fr.pa1007.calculator.exception.MissingNumberException;
import java.util.List;
import java.util.Objects;

public class Average {


    /**
     * The coefficients of the <code>mainNumber</code>.
     *
     * @since 1.0
     */
    private List<Double> coefficients;

    /**
     * The main number of the operation.
     *
     * @since 1.0
     */
    private List<Double> mainNumbers;

    /**
     * The result of the operation.
     *
     * @since 1.0
     */
    private double result;

    public Average(List<Double> coefficients, List<Double> mainNumbers) {
        this.coefficients = coefficients;
        this.mainNumbers = mainNumbers;
    }

    /**
     * @return The coefficicients of the <code>mainNumber</code>.
     * @since 1.0
     */
    public List<Double> getCoefficients() {
        return this.coefficients;
    }

    /**
     * Sets the <code>coefficients</code> field.
     *
     * @param coefficients The coefficicients of the <code>mainNumber</code>.
     * @since 1.0
     */
    public void setCoefficients(List<Double> coefficients) {
        this.coefficients = coefficients;
    }

    /**
     * @return The main number of the operation.
     * @since 1.0
     */
    public List<Double> getMainNumbers() {
        return this.mainNumbers;
    }

    /**
     * Sets the <code>mainNumbers</code> field.
     *
     * @param mainNumbers The main number of the operation.
     * @since 1.0
     */
    public void setMainNumbers(List<Double> mainNumbers) {
        this.mainNumbers = mainNumbers;
    }

    /**
     * @return The result of the operation.
     * @since 1.0
     */
    public double getResult() {
        return this.result;
    }

    /**
     * Sets the <code>result</code> field.
     *
     * @param result The result of the operation.
     * @since 1.0
     */
    public void setResult(double result) {
        this.result = result;
    }

    public double calculate() throws MissingNumberException {

        if (coefficients.size() == mainNumbers.size()) {
            double result = 0;
            for (int i = 0; i < coefficients.size(); i++) {
                double temp = coefficients.get(i) * mainNumbers.get(i);
                result += temp;
            }
            this.result = result;
            return this.result;

        }
        else {
            throw new MissingNumberException((coefficients.size() > mainNumbers.size())
                                                     ? "There is a missing Number on the first column"
                                                     : "There is a missing Coefficient on the second column");
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Average)) {
            return false;
        }
        Average average = (Average) o;
        return Double.compare(average.result, result) == 0 &&
               Objects.equals(coefficients, average.coefficients) &&
               Objects.equals(mainNumbers, average.mainNumbers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(coefficients, mainNumbers, result);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("coefficients", coefficients)
                .add("mainNumbers", mainNumbers)
                .add("result", result)
                .toString();
    }
}
