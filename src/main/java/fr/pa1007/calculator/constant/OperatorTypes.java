package fr.pa1007.calculator.constant;

public enum OperatorTypes {
    //Main
    ADD("+"),
    MINUS("-"),
    DIVIDE("/"),
    MULTIPLY("X"),
    //Other
    TENFACTOR("E"),
    POWERTWO("²"),
    OTHERPOWER("^"),
    SIN("sin"),
    COS("cos"),
    TAN("tan"),
    PERCENTAGE("%"),
    SQUARE(""),
    DEL(""),
    CANCEL(""),
    EXE("");

    private final String text;

    /**
     * @param text
     */
    OperatorTypes(final String text) {
        this.text = text;
    }

    /**
     * Get the name of the operatorType
     *
     * @return string type
     */
    public String getName() {
        return text;
    }
}
