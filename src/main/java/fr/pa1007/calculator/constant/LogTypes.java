package fr.pa1007.calculator.constant;

public enum LogTypes {

    ERROR,
    DEBUG,
    TRACE,
    INFO,
    WARN,
    FATAL
}
