package fr.pa1007.calculator.constant;

import fr.pa1007.calculator.Calculator;
import java.util.Objects;

public class Operation {

    /**
     * The result of the operation.
     *
     * @since 1.0
     */
    private double result;

    /**
     * The operationType of the operation.
     *
     * @since 1.0
     */
    private OperatorTypes operatorTypes;

    /**
     * The second number of the operation.
     *
     * @since 1.0
     */
    private double secondNumber;

    /**
     * The number of the same operation done.
     *
     * @since 1.0
     */
    private int operationNumber;

    /**
     * The last operator use.
     *
     * @since 1.0
     */
    private OperatorTypes lastOperatorType;

    /**
     * The first number of the operation.
     *
     * @since 1.0
     */
    private double firstNumber;


    public Operation(OperatorTypes operatorTypes, int operationNumber, double firstNumber) {
        this.operatorTypes = operatorTypes;
        this.operationNumber = operationNumber;
        this.firstNumber = firstNumber;
        this.lastOperatorType = operatorTypes;
    }

    public Operation(double result, OperatorTypes operatorTypes, double secondNumber, double firstNumber) {
        this.result = result;
        this.operatorTypes = operatorTypes;
        this.secondNumber = secondNumber;
        this.firstNumber = firstNumber;
        this.lastOperatorType = operatorTypes;
    }

    public Operation(OperatorTypes operatorTypes, double secondNumber, double firstNumber) {
        this.operatorTypes = operatorTypes;
        this.secondNumber = secondNumber;
        this.firstNumber = firstNumber;
        this.lastOperatorType = operatorTypes;
    }

    /**
     * @return The number of the same operation done.
     * @since 1.0
     */
    public int getOperationNumber() {
        return this.operationNumber;
    }

    /**
     * Sets the <code>operationNumber</code> field.
     *
     * @param operationNumber The number of the same operation done.
     * @since 1.0
     */
    public void setOperationNumber(int operationNumber) {
        this.operationNumber = operationNumber;
    }

    /**
     * @return The last operator use.
     * @since 1.0
     */
    public OperatorTypes getLastOperatorType() {
        return this.lastOperatorType;
    }

    /**
     * Sets the <code>lastOperatorType</code> field.
     *
     * @param lastOperatorType The last operator use.
     * @since 1.0
     */
    public void setLastOperatorType(OperatorTypes lastOperatorType) {
        this.lastOperatorType = lastOperatorType;
    }

    /**
     * @return The result of the operation.
     * @since 1.0
     */
    public double getResult() {
        return this.result;
    }

    /**
     * Sets the <code>result</code> field.
     *
     * @param result The result of the operation.
     * @since 1.0
     */
    public void setResult(double result) {
        this.result = result;
    }

    /**
     * @return The operationType of the operation.
     * @since 1.0
     */
    public OperatorTypes getOperatorTypes() {
        return this.operatorTypes;
    }

    /**
     * Sets the <code>operatorTypes</code> field.
     *
     * @param operatorTypes The operationType of the operation.
     * @since 1.0
     */
    public void setOperatorTypes(OperatorTypes operatorTypes) {
        this.operatorTypes = operatorTypes;
    }

    /**
     * @return The second number of the operation.
     * @since 1.0
     */
    public double getSecondNumber() {
        return this.secondNumber;
    }

    /**
     * Sets the <code>secondNumber</code> field.
     *
     * @param secondNumber The second number of the operation.
     * @since 1.0
     */
    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    /**
     * @return The first number of the operation.
     * @since 1.0
     */
    public double getFirstNumber() {
        return this.firstNumber;
    }

    /**
     * Sets the <code>firstNumber</code> field.
     *
     * @param firstNumber The first number of the operation.
     * @since 1.0
     */
    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public boolean compareLastOperationType(OperatorTypes operatorTypes) {
        return this.lastOperatorType == operatorTypes;
    }

    public double calculate() throws ArithmeticException {
        return this.result = Calculator.operation(lastOperatorType, firstNumber, secondNumber);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Operation)) {
            return false;
        }
        Operation operation = (Operation) o;
        return Double.compare(operation.result, result) == 0 &&
               Double.compare(operation.secondNumber, secondNumber) == 0 &&
               operationNumber == operation.operationNumber &&
               Double.compare(operation.firstNumber, firstNumber) == 0 &&
               operatorTypes == operation.operatorTypes &&
               lastOperatorType == operation.lastOperatorType;
    }

    @Override
    public int hashCode() {

        return Objects.hash(
                result,
                operatorTypes,
                secondNumber,
                operationNumber,
                lastOperatorType,
                firstNumber
        );
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("result", result)
                .add("operatorTypes", operatorTypes)
                .add("secondNumber", secondNumber)
                .add("operationNumber", operationNumber)
                .add("lastOperatorType", lastOperatorType)
                .add("firstNumber", firstNumber)
                .toString();
    }
}
