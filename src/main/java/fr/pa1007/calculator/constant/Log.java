package fr.pa1007.calculator.constant;

import javax.annotation.Nullable;
import java.util.Date;
import java.util.Objects;

public class Log {


    /**
     * The date of the log.
     *
     * @since 1.0
     */
    private Date date;

    /**
     * The type of log we want.
     *
     * @since 1.0
     */
    private LogTypes logTypes;

    /**
     * The exception to have.
     *
     * @since 1.0
     */
    @Nullable
    private String exception;

    /**
     * The main log.
     *
     * @since 1.0
     */
    private String log;

    public Log(String log, LogTypes logTypes, Date date) {
        this.log = log;
        this.logTypes = logTypes;
        this.date = date;
    }

    public Log(String log, LogTypes logTypes, @Nullable String exception, Date date) {
        this.log = log;
        this.logTypes = logTypes;
        this.exception = exception;
        this.date = date;
    }

    /**
     * @return The date of the log.
     * @since 1.0
     */
    public Date getDate() {
        return this.date;
    }

    /**
     * Sets the <code>date</code> field.
     *
     * @param date The date of the log.
     * @since 1.0
     */
    public void setDate(Date date) {
        this.date = date;
    }

    /**
     * @return The exception to have.
     * @since 1.0
     */
    @Nullable
    public String getException() {
        return this.exception;
    }

    /**
     * Sets the <code>exception</code> field.
     *
     * @param exception The exception to have.
     * @since 1.0
     */
    public void setException(@Nullable String exception) {
        this.exception = exception;
    }


    /**
     * @return The main log.
     * @since 1.0
     */
    public String getLog() {
        return this.log;
    }

    /**
     * Sets the <code>log</code> field.
     *
     * @param log The main log.
     * @since 1.0
     */
    public void setLog(String log) {
        this.log = log;
    }

    /**
     * @return The type of log we want.
     * @since 1.0
     */
    public LogTypes getLogTypes() {
        return this.logTypes;
    }

    /**
     * Sets the <code>logTypes</code> field.
     *
     * @param logTypes The type of log we want.
     * @since 1.0
     */
    public void setLogTypes(LogTypes logTypes) {
        this.logTypes = logTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Log)) {
            return false;
        }
        Log log1 = (Log) o;
        return Objects.equals(log, log1.log) &&
               logTypes == log1.logTypes &&
               Objects.equals(exception, log1.exception) &&
               Objects.equals(date, log1.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(log, logTypes, exception, date);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("date", date)
                .add("logTypes", logTypes)
                .add("log", log)
                .add("exception", exception)
                .toString();
    }
}
