package fr.pa1007.calculator.constant;

import java.util.Objects;

public class AverageTableRow {


    /**
     * The main number of the row.
     *
     * @since 1.0
     */
    private double mainNumber;

    /**
     * The coefficient of the main number.
     *
     * @since 1.0
     */
    private double coefNumber;

    public AverageTableRow(double mainNumber, double coefNumber) {
        this.mainNumber = mainNumber;
        this.coefNumber = coefNumber;
    }

    /**
     * @return The coefficient of the main number.
     * @since 1.0
     */
    public double getCoefNumber() {
        return this.coefNumber;
    }

    /**
     * Sets the <code>coefNumber</code> field.
     *
     * @param coefNumber The coefficient of the main number.
     * @since 1.0
     */
    public void setCoefNumber(double coefNumber) {
        this.coefNumber = coefNumber;
    }

    /**
     * @return The main number of the row.
     * @since 1.0
     */
    public double getMainNumber() {
        return this.mainNumber;
    }

    /**
     * Sets the <code>mainNumber</code> field.
     *
     * @param mainNumber The main number of the row.
     * @since 1.0
     */
    public void setMainNumber(double mainNumber) {
        this.mainNumber = mainNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AverageTableRow)) {
            return false;
        }
        AverageTableRow that = (AverageTableRow) o;
        return Double.compare(that.mainNumber, mainNumber) == 0 &&
               Double.compare(that.coefNumber, coefNumber) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(mainNumber, coefNumber);
    }

    @Override
    public String toString() {
        return com.google.common.base.Objects.toStringHelper(this)
                .add("mainNumber", mainNumber)
                .add("coefNumber", coefNumber)
                .toString();
    }
}
