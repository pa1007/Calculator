package fr.pa1007.calculator.controller;

import com.jfoenix.controls.JFXTextField;
import fr.pa1007.calculator.Calculator;
import fr.pa1007.calculator.constant.Average;
import fr.pa1007.calculator.constant.AverageTableRow;
import fr.pa1007.calculator.constant.Operation;
import fr.pa1007.calculator.constant.OperatorTypes;
import fr.pa1007.calculator.creator.Logger;
import fr.pa1007.calculator.exception.MissingNumberException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainSceneController {

    @FXML
    public  GridPane                        gridPane;
    @FXML
    public  JFXTextField                    numberPercentage;
    @FXML
    public  JFXTextField                    coefAverage;
    @FXML
    public  JFXTextField                    numberAverage;
    public  JFXTextField                    hexaScreen;
    public  JFXTextField                    binaryScreen;
    private boolean                         operatorSet   = false;
    private List<Operation>                 lastOperation = new ArrayList<>();
    private double                          ans           = 0;
    private Operation                       lastestOperation;
    @FXML
    private TextField                       screen;
    @FXML
    private TextField                       errorScreen;
    private Stage                           mainStage;
    @FXML
    private VBox                            vbox;
    @FXML
    private TableView<AverageTableRow>      averageTable;
    private boolean                         grow;
    private ObservableList<AverageTableRow> data          = FXCollections.observableArrayList();
    private Calculator                      instance      = Calculator.getINSTANCE();
    private Average                         lastestAverage;
    private List<String>                    allowedString = Arrays.asList("+", ".", "-", "X", "/", "*", " ");

    public List<Operation> getLastOperation() {
        return lastOperation;
    }

    public double getAns() {
        return ans;
    }

    public void setAns(double ans) {
        this.ans = ans;
    }

    @FXML
    public void addHandler(ActionEvent actionEvent) {
        if (isOperatorSet()) {
            String[] op = screen.getText().split(" ");
            if (op.length != 3) {
                errorScreen.setText("ERROR too much space between number");
            }
            try {
                lastestOperation.setSecondNumber(Double.parseDouble(op[2]));
                screen.setText(String.valueOf(lastestOperation.calculate()) + " + ");
                getLastOperation().add(lastestOperation);
                ans = lastestOperation.getFirstNumber();
                setLastestOperation(new Operation(OperatorTypes.ADD, 1, lastestOperation.getResult()));

            }
            catch (Exception e) {
                errorScreen.setText("ERROR WHILE CALCULATING RESULT");
                Logger.e("ERROR WHILE CALCULATING RESULT : " + lastestOperation.toString());
            }

        }
        else {
            try {
                setLastestOperation(new Operation(OperatorTypes.ADD, 1, Double.valueOf(screen.getText())));
                setOperatorSet(true);
                screen.setText(screen.getText() + " + ");
            }
            catch (NumberFormatException e) {
                errorScreen.setText("ERROR WHILE FORMATTING NUMBER");
                Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
            }
        }
    }

    @FXML
    public void minusHandler(ActionEvent actionEvent) {
        if (isOperatorSet()) {
            String[] op = screen.getText().split(" ");
            if (op.length != 3) {
                errorScreen.setText("ERROR too much space between number");
            }
            lastestOperation.setSecondNumber(Double.parseDouble(op[2]));
            try {
                screen.setText(String.valueOf(lastestOperation.calculate()) + " - ");
                getLastOperation().add(lastestOperation);
                ans = lastestOperation.getFirstNumber();
                setLastestOperation(new Operation(OperatorTypes.MINUS, 1, lastestOperation.getResult()));

            }
            catch (ArithmeticException e) {
                errorScreen.setText("ERROR WHILE CALCULATING RESULT");
                Logger.e("ERROR WHILE CALCULATING RESULT : " + lastestOperation.toString());
            }

        }
        else {
            try {
                setLastestOperation(new Operation(OperatorTypes.MINUS, 1, Double.valueOf(screen.getText())));
                setOperatorSet(true);
                screen.setText(screen.getText() + " - ");
            }
            catch (ArithmeticException e) {
                Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
            }
        }
    }

    public boolean isGrow() {
        return grow;
    }

    public void setGrow(boolean grow) {
        this.grow = grow;
    }

    public Stage getMainStage() {
        return mainStage;
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
    }

    @FXML
    public void ansHandler(ActionEvent event) {
        screen.setText(screen.getText() + ((getAns() == 0.0) ? "" : ans));
    }

    @FXML
    public void averageHandler(ActionEvent event) {
        ObservableList<AverageTableRow> tempData = averageTable.getItems();
        List<Double>                    coef     = new ArrayList<>(), numbers = new ArrayList<>();
        for (AverageTableRow averageTableRow : tempData) {
            coef.add(averageTableRow.getCoefNumber());
            numbers.add(averageTableRow.getMainNumber());
        }
        lastestAverage = new Average(coef, numbers);
        try {
            if (isOperatorSet()) {
                screen.setText(screen.getText() + String.valueOf(lastestAverage.calculate()));
            }
            else {
                screen.setText(String.valueOf(lastestAverage.calculate()));
            }
        }
        catch (MissingNumberException e) {
            errorScreen.setText(e.getMessage());
            Logger.e(e.getMessage());
        }
        data.clear();
        averageTable.setItems(data);
    }

    @FXML
    public void percentHandler(ActionEvent event) {
        if (!operatorSet) {
            if (!numberPercentage.getText().equals("")) {
                if (!screen.getText().equals("")) {
                    try {
                        double percentage  = Double.parseDouble(numberPercentage.getText());
                        double firstNumber = Double.parseDouble(screen.getText());
                        double result;
                        if (percentage < -1 || percentage > 1) {
                            if (percentage > 0) {
                                result = firstNumber * ((percentage / 100) + 1);
                            }
                            else {
                                percentage = percentage + 100;
                                result = firstNumber * (percentage / 100); //150 * 0.2
                            }
                        }
                        else if (percentage > -1 && percentage < 1) {
                            if (percentage > 0) {
                                result = firstNumber * percentage;
                            }
                            else {
                                percentage = percentage * -1;
                                result = firstNumber * percentage;
                            }
                        }
                        else {
                            result = firstNumber;
                        }
                        screen.setText(String.valueOf(result));
                        ans = firstNumber;
                        lastOperation.add(new Operation(result, OperatorTypes.PERCENTAGE, firstNumber, percentage));


                    }
                    catch (NumberFormatException e) {
                        errorScreen.setText("Number formation ERROR");
                        Logger.e("Wrong Format used", e.getMessage());
                    }
                }
            }
        }
    }

    @FXML
    public void addAverageHandler(ActionEvent event) {
        if (!coefAverage.getText().equals("") && !numberAverage.getText().equals("")) {
            try {
                data.add(new AverageTableRow(
                        Double.valueOf(numberAverage.getText()),
                        Double.valueOf(coefAverage.getText())
                ));
            }
            catch (NumberFormatException e) {
                errorScreen.setText("Error with the number formation");
                Logger.e("Wrong Format used", e.getMessage());
            }
            averageTable.setItems(data);
        }

        coefAverage.setText("");
        numberAverage.setText("");
    }

    @FXML
    public void factorialHandler(ActionEvent event) {
        if (screen.getText().equals("")) {
            return;
        }
        try {
            setAns(Double.parseDouble(screen.getText()));
            long result = 1;
            int  start  = (int) Double.parseDouble(screen.getText());
            if (start < 0) {
                errorScreen.setText("The number must be upper than 0");
                start *= -1;
                screen.setText(String.valueOf(start));
                return;
            }
            for (int i = 2; i <= start; i++) {
                result *= i;
            }
            screen.setText(String.valueOf(result));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("Wrong Format used", e.getMessage());
        }
    }

    @FXML
    public void actionErrorScreen(ActionEvent event) {
        System.out.println("test");
    }

    @FXML
    public void reCalculateHandler(ActionEvent event) {
        if (screen.getText().equals("")) {
            if (binaryScreen.getText().equals("")) {
                if (hexaScreen.getText().equals("")) {
                }
                else {
                    try {
                        String number = hexaScreen.getText();
                        screen.setText(String.valueOf(Integer.parseInt(number, 16)));
                        binaryScreen.setText((Integer.toBinaryString(Integer.parseInt(screen.getText()))));
                    }
                    catch (ArithmeticException | NumberFormatException e) {
                        Logger.e("FORMAT EXCEPTION : " + screen.getText());
                    }
                }
            }
            else {
                String number = binaryScreen.getText();
                try {
                    screen.setText(String.valueOf(Integer.parseInt(number, 2)));
                    hexaScreen.setText(Integer.toHexString(Integer.parseInt(screen.getText())));
                }
                catch (ArithmeticException | NumberFormatException e) {
                    Logger.e("FORMAT EXCEPTION : " + screen.getText());
                }
            }
        }
        else {
            try {
                int number = (int) Double.parseDouble(screen.getText());
                binaryScreen.setText(Integer.toBinaryString(number));
                hexaScreen.setText(Integer.toHexString(number));
            }
            catch (ArithmeticException | NumberFormatException e) {
                Logger.e("FORMAT EXCEPTION : " + screen.getText());
            }
        }

    }

    public void lnHandler(ActionEvent event) {
        try {
            setAns(Double.parseDouble(screen.getText()));
            screen.setText(String.valueOf(Math.log(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    public void eHandler(ActionEvent event) {
        try {
            setAns(Double.parseDouble(screen.getText()));
            screen.setText(String.valueOf(Math.exp(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    public void logHandler(ActionEvent event) {
        try {
            setAns(Double.parseDouble(screen.getText()));
            screen.setText(String.valueOf(Math.log10(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    public void logOpHandler(ActionEvent event) {
        try {
            setAns(Double.parseDouble(screen.getText()));
            screen.setText(String.valueOf(Math.pow(10.0, Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void eightHandler(ActionEvent event) {
        screen.setText(screen.getText() + "8");
    }

    @FXML
    void nineHandler(ActionEvent event) {
        screen.setText(screen.getText() + "9");
    }

    @FXML
    void fourHandler(ActionEvent event) {
        screen.setText(screen.getText() + "4");
    }

    @FXML
    void fiveHandler(ActionEvent event) {
        screen.setText(screen.getText() + "5");
    }

    @FXML
    void sixHandler(ActionEvent event) {
        screen.setText(screen.getText() + "6");
    }

    @FXML
    void treeHandler(ActionEvent event) {
        screen.setText(screen.getText() + "3");
    }

    @FXML
    void twoHandler(ActionEvent event) {
        screen.setText(screen.getText() + "2");
    }

    @FXML
    void oneHandler(ActionEvent event) {
        screen.setText(screen.getText() + "1");
    }

    @FXML
    void zeroHandler(ActionEvent event) {
        screen.setText(screen.getText() + "0");
    }

    @FXML
    void cleanHandler(ActionEvent event) {
        screen.setText("");
        setLastestOperation(null);
        setOperatorSet(false);
    }

    @FXML
    void squareHandler(ActionEvent event) {
        try {
            setAns(Double.parseDouble(screen.getText()));
            screen.setText(String.valueOf(Math.sqrt(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR INVALID TYPE");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void commaHandler(ActionEvent event) {
        screen.setText(screen.getText() + ".");
    }

    @FXML
    void cosHandler(ActionEvent event) {
        try {
            ans = Double.parseDouble(screen.getText());
            screen.setText(String.valueOf(Math.cos(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void cubeHandler(ActionEvent event) {
        try {
            ans = Double.parseDouble(screen.getText());
            screen.setText(String.valueOf(Calculator.power(Double.parseDouble(screen.getText()), 2)));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void tanHandler(ActionEvent event) {
        try {
            ans = Double.parseDouble(screen.getText());
            screen.setText(String.valueOf(Math.tan(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void sinHandler(ActionEvent event) {
        try {
            ans = Double.parseDouble(screen.getText());
            screen.setText(String.valueOf(Math.sin(Double.parseDouble(screen.getText()))));
        }
        catch (NumberFormatException e) {
            errorScreen.setText("ERROR");
            Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
        }
    }

    @FXML
    void delHandler(ActionEvent event) {
        try {
            screen.setText(screen.getText(0, screen.getText().length() - 1));
        }
        catch (Exception ignored) {

        }
    }

    @FXML
    void pourHandler(ActionEvent event) {
        vbox.getStyleClass().clear();
        if (isGrow()) {
            setGrow(false);
            vbox.getStyleClass().add("fillout");
            mainStage.setWidth(mainStage.getWidth() - 400);
        }
        else {
            vbox.getStyleClass().add("grow");
            setGrow(true);
            mainStage.setWidth(mainStage.getWidth() + 400);
        }
        Logger.i("Scene Width change to " + mainStage.getWidth());
    }

    @FXML
    void negateHandler(ActionEvent event) {
        screen.setText(screen.getText() + "-");
    }

    @FXML
    void divHandler(ActionEvent event) {
        if (isOperatorSet()) {
            String[] op = screen.getText().split(" ");
            if (op.length > 3) {
                errorScreen.setText("ERROR too much space between number");
            }
            lastestOperation.setSecondNumber(Double.parseDouble(op[2]));
            try {
                screen.setText(String.valueOf(lastestOperation.calculate()) + " / ");
                getLastOperation().add(lastestOperation);
                ans = lastestOperation.getFirstNumber();
                setLastestOperation(new Operation(OperatorTypes.DIVIDE, 1, lastestOperation.getResult()));

            }
            catch (ArithmeticException e) {
                errorScreen.setText("ERROR WHILE CALCULATING RESULT");
                Logger.e("ERROR WHILE CALCULATING RESULT : " + lastestOperation.toString());
            }

        }
        else {
            try {
                setLastestOperation(new Operation(OperatorTypes.DIVIDE, 1, Double.valueOf(screen.getText())));
                setOperatorSet(true);
                screen.setText(screen.getText() + " / ");
            }
            catch (NumberFormatException e) {
                Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
            }
        }
    }

    @FXML
    void mulHandler(ActionEvent event) {
        if (isOperatorSet()) {
            String[] op = screen.getText().split(" ");
            if (op.length != 3) {
                errorScreen.setText("ERROR too much space between number");
            }
            lastestOperation.setSecondNumber(Double.parseDouble(op[2]));
            try {
                screen.setText(String.valueOf(lastestOperation.calculate()) + " X ");
                getLastOperation().add(lastestOperation);
                ans = lastestOperation.getFirstNumber();
                setLastestOperation(new Operation(OperatorTypes.MULTIPLY, 1, lastestOperation.getResult()));

            }
            catch (ArithmeticException e) {
                errorScreen.setText("ERROR WHILE CALCULATING RESULT");
                Logger.e("ERROR WHILE CALCULATING RESULT : " + lastestOperation.toString());
            }

        }
        else {
            try {
                setLastestOperation(new Operation(OperatorTypes.MULTIPLY, 1, Double.valueOf(screen.getText())));
                setOperatorSet(true);
                screen.setText(screen.getText() + " X ");
            }
            catch (NumberFormatException e) {
                errorScreen.setText("Number format exception");
                Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
            }
        }
    }

    @FXML
    void powerTenHandler(ActionEvent event) {
        screen.setText(screen.getText() + "E");
    }

    @FXML
    void screenHandler(KeyEvent event) {
        if (!allowedString.contains(event.getCharacter())) {
            try {
                Double.valueOf(event.getCharacter());
            }
            catch (NumberFormatException e) {
                event.consume();
            }
        }
        else {
            if (event.getCharacter().equals("*")) {
                event.consume();
                screen.setText(screen.getText() + "X");
            }
        }
    }

    @FXML
    void exeHandler(ActionEvent event) {
        if (isOperatorSet()) {
            String[] op = screen.getText().split(" ");
            if (op.length != 3) {
                errorScreen.setText("too much space between number");
                Logger.i("Too much space in the operation");
            }
            lastestOperation.setSecondNumber(Double.parseDouble(op[2]));
            try {
                screen.setText(String.valueOf(lastestOperation.calculate()));
                setLastestOperation(null);
                setOperatorSet(false);
            }
            catch (NumberFormatException e) {
                Logger.e("ERROR WHILE FORMATTING NUMBER : " + screen.getText());
                errorScreen.setText("ERROR");
            }
            setOperatorSet(false);

        }
        else {
            String[] op = screen.getText().split(" ");
            if (op.length == 3) {
                OperatorTypes type;
                switch (op[1]) {
                    case "+":
                        type = OperatorTypes.ADD;
                        break;
                    case "-":
                        type = OperatorTypes.MINUS;
                        break;
                    case "/":
                        type = OperatorTypes.DIVIDE;
                        break;
                    case "X":
                        type = OperatorTypes.MULTIPLY;
                        break;
                    default:
                        errorScreen.setText("Error with operator");
                        Logger.e("ERROR WITH OPERATOR FORMAT: " + lastestOperation.toString());
                        return;
                }

                Operation temp = new Operation(type, Double.valueOf(op[2]), Double.valueOf(op[0]));
                screen.setText(String.valueOf(temp.calculate()));
                this.lastOperation.add(temp);

            }
            else {
                errorScreen.setText("ERROR while formatting operation");
                Logger.w("WRONG FORMAT");
            }
        }
    }

    @FXML
    private void sevenHandler(ActionEvent event) {
        screen.setText(screen.getText() + "7");
    }

    private Operation getLastestOperation() {
        return lastestOperation;
    }

    private void setLastestOperation(Operation lastestOperation) {
        this.lastestOperation = lastestOperation;
    }

    private boolean isOperatorSet() {
        return operatorSet;
    }

    private void setOperatorSet(boolean operatorSet) {
        this.operatorSet = operatorSet;
    }
}