package fr.pa1007.calculator.creator;

import fr.pa1007.calculator.Calculator;
import fr.pa1007.calculator.constant.Log;
import fr.pa1007.calculator.constant.LogTypes;
import java.time.Instant;
import java.util.Date;

public class Logger {

    /**
     * To create a generic log with given type
     *
     * @param logTypes the type of error
     * @param log      the log to show
     */
    public static void createLog(LogTypes logTypes, String log) {
        Calculator.getLogList().add(new Log(log, logTypes, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To create a generic log with given type
     *
     * @param logTypes the type of error
     * @param log      the log to show
     * @param error    the main error
     */
    public static void createLog(LogTypes logTypes, String log, String error) {
        Calculator.getLogList().add(new Log(log, logTypes, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Debug log
     *
     * @param log   the log to wrote
     * @param error The reported error
     */
    public static void d(String log, String error) {
        Calculator.getLogList().add(new Log(log, LogTypes.DEBUG, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Error log
     *
     * @param log   the log to wrote
     * @param error The reported error
     */
    public static void e(String log, String error) {
        Calculator.getLogList().add(new Log(log, LogTypes.ERROR, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Fatal log
     *
     * @param log   the log to wrote
     * @param error The reported error
     */
    public static void f(String log, String error) {
        Calculator.getLogList().add(new Log(log, LogTypes.FATAL, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Info log
     *
     * @param log   the log to wrote
     * @param error The reported error
     */
    public static void i(String log, String error) {
        Calculator.getLogList().add(new Log(log, LogTypes.INFO, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Warn log
     *
     * @param log   the log to wrote
     * @param error The reported error
     */
    public static void w(String log, String error) {
        Calculator.getLogList().add(new Log(log, LogTypes.WARN, error, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Debug log
     *
     * @param log the log to wrote
     */
    public static void d(String log) {
        Calculator.getLogList().add(new Log(log, LogTypes.DEBUG, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Error log
     *
     * @param log the log to wrote
     */
    public static void e(String log) {
        Calculator.getLogList().add(new Log(log, LogTypes.ERROR, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Fatal log
     *
     * @param log the log to wrote
     */
    public static void f(String log) {
        Calculator.getLogList().add(new Log(log, LogTypes.FATAL, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Info log
     *
     * @param log the log to wrote
     */
    public static void i(String log) {
        Calculator.getLogList().add(new Log(log, LogTypes.INFO, Date.from(Instant.now())));
        Calculator.saveLog();
    }

    /**
     * To easily create a Warn log
     *
     * @param log the log to wrote
     */
    public static void w(String log) {
        Calculator.getLogList().add(new Log(log, LogTypes.WARN, Date.from(Instant.now())));
        Calculator.saveLog();
    }
}
